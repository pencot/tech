# Penfield Cottage Technology

## Domains

- `penfieldcottage.com`
  - Registration: gandi.net (Penfield Cottage Account)
  - DNS: CloudFlare (Penfield Cottage Account)
  - Email: gandi.net

## Documentation

- https://gitlab.com/pencot/tech
  - Repository contains public information about technology in use
  - Project wiki holds internal-only information

## Group Accounts

- gandi.net Penfield Cottage Account
  - Talk to Ryan or John for access
- CloudFlare Penfield Cottage Account
  - Most updates should be made via infrastructure-as-code
  - Talk to Ryan for group access
