# Penfield Cottage Code Platform

We need a way to store tasks, code, technical notes, configuration, and infrastructure-as-code.

[[_TOC_]]

## Features

### Requirements

- Git hosting
- Code reviews
- Issue tracking
- Decent content preview, e.g. markdown content

### Highly Desirable

- Integration with PenCot single-sign-on
- Good issue management (epics, etc.)
- Wiki support
- Build system

### Extra Features

Services that could be bundled with other systems:

- Chat system

## Options

### Recommended

### Exploring

##### Phabricator

Appears to be a slick interface, with a promising looking wiki.  
Customizable task management may be able to help other groups, e.g. tracking issues around the property.

Points to explore:
- Is it really as slick/easy to use as it appears
- Does the wiki actually work better than GitLab (e.g. is it more Confluence or GitLab)
- Can the build system work well enough for us

### Other

##### Self-hosted GitLab

Pros:

- SSO support
- Good (but not mindblowing) platform

Cons:

- Limited issue tracking, wiki

##### gitlab.com

Pros:

- Free
- Good (but not mindblowing) platform

Cons:

- No PenCot SSO integration (but could be added with an on-site deployment)
- Issue tracking, wiki is limited

### Rejected
