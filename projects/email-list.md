# Penfield Cottage Mailing List

The family needs a mailing list for family communication.

[[_TOC_]]

## Features

### Requirements

- Secure - family-only access
- Reliable email delivery
- Post by emailing the group
- Cheap

### Highly Desirable

- Web interface for browsing
- Web interface for posting
- Easy to have sub-groups/multiple groups - e.g. committees

### Optional

- Integration with common authentication

### Unknown Desirability

- Posting queue/moderation (pending messages)
- Public requests to join (e.g. pending members)

## Options

### Suggested

#### Google Groups

Pros:

- Free
- Powerful web interface
- Very reliable email delivery

Cons:

- Requires Google accounts (doesn't require `gmail.com`, but a linked Google account)
- Sub-groups are bad, but easy to create multiple groups

#### Discourse

https://discourse.org

Pros:

- Cheap (self-hosted)
- Very powerful web interface
- Easy to have multiple groups all in the same system
- Would integrate with common authentication system

Cons:

- Has server costs
- Requires setup/management/maintenance for self-hosted
- Likely fine, but potentially worse reliable email delivery

### Other Options

### Rejected

#### Previous System (BizLand)

- No web interface
- Ties domain to BizLand, which prevents other projects
